(function($) {
    var irrigacoes = [];

    function updateUmidade(umidade, seletor){
        var idAntigo = $(seletor).attr("data-id");
        if(umidade.id > idAntigo){
            $(seletor).html(umidade.umidade);
            $(seletor).attr("data-id", umidade.id);

            //
            console.log("Novo id: " + umidade.id);
            //
        }
        // else{
            // console.log("Erro: id antigo = " + idAntigo + ", id novo = " + umidade.id + ", seletor: " + seletor);
        // }
    }

    function getLastUmidade(planta, seletor){
        $.ajax({
            method: "GET",
            url: "/control/umidade.php?type=max&planta=" + planta,
            success: function(json){
                var umidade = JSON.parse(json);
                updateUmidade(umidade, seletor);
            }
        });
    }

    //IRRIGACAO=================================================================

    Array.prototype.inArray = function (comparer){
        for(var i=0; i < this.length; i++){
            if(comparer(this[i])) return true;
        }
        return false;
    }

    Array.prototype.pushIfNotExist = function(element, comparer) {
        if (!this.inArray(comparer)) {
            this.push(element);
        }
    };

    function getLastIrrigacao(){
        $.ajax({
            method: "GET",
            url: "/control/irrigacao.php?type=max",
            success: function(json){
                var size = irrigacoes.length;
                var irrigacao = JSON.parse(json);
                irrigacoes.pushIfNotExist(irrigacao, function(e){
                    return e.id == irrigacao.id;
                });

                if(irrigacoes.length > size){
                    addIrrigacao(irrigacao);
                }
            }
        });
    }

    function addIrrigacao(){

    }


    $(document).ready(function(){
        setInterval(function(){
            getLastUmidade("a", ".p1");
        }, 5000);
    })
})(jQuery);
