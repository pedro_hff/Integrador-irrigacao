<?php
include_once("../model/Irrigacao.php");

include_once("../dao/IrrigacaoDAO.php");

$dao = new IrrigacaoDAO;
if(isset($_GET)){
    //?type=insert&umidadeInicial=33&umidadeFinal=80&planta=a
    if($_GET["type"] == "insert"){
        $i = new Irrigacao;
        $i->getCurrentTime();
        $i->umidadeInicial = $_GET["umidadeInicial"];
        $i->umidadeFinal = $_GET["umidadeFinal"];
	    $i->planta = $_GET["planta"];

        if($dao->insert($i)){
            echo "{
                \"id\": \"$i->id\",
                \"data\": \"$i->data\",
                \"umidadeInicial\": \"$i->umidadeInicial\",
                \"umidadeFinal\": \"$i->umidadeFinal\",
		        \"planta\": \"$i->planta\",
                \"erro\": \"false\"
            }";
        }else{
            echo "{
                \"erro\": \"true\"
            }";
        }
    }else{
        //?type=max
        if($_GET["type"] == "max"){
            $i = $dao->getLast();
            if($i != null){
                echo "{
                    \"id\": \"$i->id\",
                    \"data\": \"$i->data\",
                    \"umidadeInicial\": \"$i->umidadeInicial\",
                    \"umidadeFinal\": \"$i->umidadeFinal\",
		            \"planta\": \"$i->planta\",
                    \"erro\": \"false\"
                }";
            }else{
                echo "{
                    \"erro\": \"true\"
                }";
            }
        }else{
            //?type=get&id=12
            if($_GET["type"] == "get"){
                $i = $dao->findById($_GET["id"]);
                if($i != null){
                    echo "{
                        \"id\": \"$i->id\",
                        \"data\": \"$i->data\",
                        \"umidadeInicial\": \"$i->umidadeInicial\",
                        \"umidadeFinal\": \"$i->umidadeFinal\",
			            \"planta\": \"$i->planta\",
                        \"erro\": \"false\"
                    }";
                }else{
                    echo "{
                        \"erro\": \"true\"
                    }";
                }
            }


        }
    }
}

 ?>
