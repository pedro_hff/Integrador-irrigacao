<?php
include_once("../model/Umidade.php");
include_once("../model/Graph.php");
include_once("../dao/UmidadeDAO.php");

$dao = new UmidadeDAO;
if(isset($_GET)){
    //?type=insert&planta=A&umidade=32
    if($_GET["type"] == "insert"){
        $u = new Umidade;
        $u->getCurrentTime();
        $u->planta = $_GET["planta"];
        $u->umidade = $_GET["umidade"];
        if(($a = $dao->insert($u))->id != null){
            echo "{
                \"id\": \"$u->id\",
                \"planta\": \"$u->planta\",
                \"umidade\": \"$u->umidade\",
                \"data\": \"$u->data\",
                \"erro\": \"false\"
            }";
        }else{
            echo "{
                \"erro\": \"$a\"
            }";
        }
    }else{
        //?type=max&planta=A
        if($_GET["type"] == "max"){
            $u = $dao->getLast($_GET["planta"]);
            if($u != null){
                echo "{
                    \"id\": \"$u->id\",
                    \"planta\": \"$u->planta\",
                    \"umidade\": \"$u->umidade\",
                    \"data\": \"$u->data\",
                    \"erro\": \"false\"
                }";
            }else{
                echo "{
                    \"erro\": \"true\"
                }";
            }
        }else{
            //?type=get&id=123
            if($_GET["type"] == "get"){
                $u= $dao->findById($_GET["id"]);
                if($u != null){
                    echo "{
                        \"id\": \"$u->id\",
                        \"planta\": \"$u->planta\",
                        \"umidade\": \"$u->umidade\",
                        \"data\": \"$u->data\",
                        \"erro\": \"false\"
                    }";
                }else{
                    echo "{
                        \"erro\": \"true\"
                    }";
                }
            }else{

                if($_GET["type"] == "graph"){
                    $u = $dao->getGraph($_GET["planta"]);
                    if($u != null){
                        echo $u;
                    }
                }
            }


        }
    }
}

 ?>
