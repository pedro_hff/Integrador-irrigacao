#include <SPI.h>
#include <Ethernet.h>

//Configuracao de internet
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//Change to your server domain
char serverName[] = "192.168.1.100";
IPAddress server(192,168,1,100);
// change to your server's port
int serverPort = 80;
IPAddress ip(192,168,1,101);
IPAddress gateway(182,168,1,1);
IPAddress subnet(255,255,255,0);
EthernetClient client;
int totalCount = 0; 
// insure params is big enough to hold your variables
char pageAdd[64];

//configuracao de tempo
#define delayMillis 3000UL
unsigned long thisMillis = 0;
unsigned long lastMillis = 0;


//variaveis de valor maximo e minimo da umidade
int umidadeMin = 40;
int umidadeMax = 70;

void setup() {
  Serial.begin(9600);
  //Rele
  //pinMode(5,OUTPUT);  
  
  Serial.print(F("Starting ethernet..."));
  if(!Ethernet.begin(mac)) Serial.println(F("failed"));
  else Serial.println(Ethernet.localIP());


  delay(2000);
  Serial.println(F("Ready"));
}

void loop()
{
  // If using a static IP, comment out the next line
  Ethernet.maintain();

  thisMillis = millis();

  if(thisMillis - lastMillis > delayMillis)
  {
    lastMillis = thisMillis;
    
    verificarUmidade("a", A0);
    //verificarUmidade("b", A1);
    
    // params must be url encoded.
    //sprintf(params,"msg=\"teste%i\"",totalCount);   
    //if(!postPage(serverName,serverPort,pageName,params)) Serial.print(F("Fail "));
    //else Serial.print(F("Pass "));
    totalCount++;
    Serial.println(totalCount,DEC);
  }    
}

void verificarUmidade(char *planta, int analogPort){
  int porcentagem = calculaPorcentagem(analogPort);
  if(porcentagem < umidadeMin){
    int umidadeInicial = porcentagem;
    int umidadeFinal;
    
    while(calculaPorcentagem(analogPort) < umidadeMax){
      //Liga rele aqui
      delay(3000);
      //Desliga rele aqui
    }
    umidadeFinal = calculaPorcentagem(analogPort);
    insertIrrigacao(umidadeInicial, umidadeFinal, planta);
  }else{
    insertUmidade(planta, calculaPorcentagem(analogPort));
  }
}

int calculaPorcentagem(int analogPort){
  return map(analogRead(analogPort), 1023, 0, 0, 100);
}


void insertIrrigacao(int umidadeInicial, int umidadeFinal, char *planta){
  char req[128];
  //?type=insert&umidadeInicial=33&umidadeFinal=80&planta=a
  sprintf(req,"/control/irrigacao.php?type=insert&umidadeInicial=%d&umidadeFinal=%d&planta=%s", umidadeInicial, umidadeFinal, planta);
  
  getPage(server,serverPort,req);
}

void insertUmidade(char *planta, int u){
  char req[128];
  //?type=insert&planta=A&umidade=32fffasdfasdfasdfasdfasdfasdfasdf
  sprintf(req,"/control/umidade.php?type=insert&planta=%s&umidade=%d", planta, u);
  
  getPage(server,serverPort,req);
}

byte getPage(IPAddress ipBuf,int thisPort, char *page){
  int inChar;
  char outBuf[128];

  Serial.print(F("connecting..."));

  if(client.connect(ipBuf,thisPort) == 1)
  {
    Serial.println(F("connected"));

    sprintf(outBuf,"GET %s HTTP/1.1",page);
    client.println(outBuf);
    sprintf(outBuf,"Host: %s",serverName);
    client.println(outBuf);
    client.println(F("Connection: close\r\n"));
  } 
  else
  {
    Serial.println(F("failed"));
    return 0;
  }

  // connectLoop controls the hardware fail timeout
  int connectLoop = 0;

  while(client.connected())
  {
    while(client.available())
    {
      inChar = client.read();
      Serial.write(inChar);
      // set connectLoop to zero if a packet arrives
      connectLoop = 0;
    }

    connectLoop++;

    // if more than 10000 milliseconds since the last packet
    if(connectLoop > 10000)
    {
      // then close the connection from this end.
      Serial.println();
      Serial.println(F("Timeout"));
      client.stop();
    }
    // this is a delay for the connectLoop timing
    delay(1);
  }

  Serial.println();

  Serial.println(F("disconnecting."));
  // close client end
  client.stop();

  return 1;
}
