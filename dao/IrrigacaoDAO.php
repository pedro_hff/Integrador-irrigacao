<?php
class IrrigacaoDAO{
    public $con;


    public function findById($id){
        $this->conOpen();
        $query = "SELECT * FROM Irrigacao WHERE id = $id ;";
        if($result = mysqli_query($this->con, $query)){
            $result = mysqli_fetch_assoc($result);
            $irrigacao = new Irrigacao;
            $irrigacao->id = $result['id'];
            $irrigacao->setTime($result['data']);
            $irrigacao->umidadeInicial = $result['umidadeInicial'];
            $irrigacao->umidadeFinal = $result['umidadeFinal'];
	        $irrigacao->planta = $result['planta'];
            return $irrigacao;
        }

        $this->conClose();
    }

    public function getLast(){
        $this->conOpen();
        $query = "SELECT * FROM Irrigacao ORDER BY id DESC LIMIT 1";
        if($result = mysqli_query($this->con, $query)){
            $result = mysqli_fetch_assoc($result);
            $irrigacao = new Irrigacao;
            $irrigacao->id = $result['id'];
            $irrigacao->setTime($result['data']);
            $irrigacao->umidadeInicial = $result['umidadeInicial'];
            $irrigacao->umidadeFinal = $result['umidadeFinal'];
	        $irrigacao->planta = $result['planta'];
            return $irrigacao;
        }

        $this->conClose();
    }

    public function insert(Irrigacao $i){
        $this->conOpen();
        $id = null;
        $query = "INSERT INTO Irrigacao (data,umidadeInicial,umidadeFinal, planta) VALUES (\"$i->data\", $i->umidadeInicial, $i->umidadeFinal,\"$i->planta\");";
        if(($this->con)->query($query) === TRUE){
            $i->id = ($this->con)->insert_id;
            $this->conClose();
            return $i;
        }else{
            return $query;
        }
    }

    public function conOpen(){
        $this->con = new mysqli("localhost", "root", "pedro123", "irrigacao");
    }

    public function conClose(){
        mysqli_close($this->con);
    }
}

 ?>
