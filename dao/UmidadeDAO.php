<?php
class UmidadeDAO{
    public $con;


    public function findById($id){
        $this->conOpen();
        $query = "SELECT * FROM Umidade WHERE id = $id ;";
        if($result = mysqli_query($this->con, $query)){
            $result = mysqli_fetch_assoc($result);
            $umidade = new Umidade;
            $umidade->id = $result['id'];
            $umidade->planta = $result['planta'];
            $umidade->umiadde = $result['umidade'];
            $umidade->setTime($result['data']);
            return $umidade;
        }

        $this->conClose();
    }

    public function getLast($planta){
        $this->conOpen();
        $query = "SELECT * FROM Umidade WHERE planta = \"$planta\" ORDER BY id DESC LIMIT 1;";
        if($result = mysqli_query($this->con, $query)){
            $result = mysqli_fetch_assoc($result);
            $umidade = new Umidade;
            $umidade->id = $result['id'];
            $umidade->planta = $result['planta'];
            $umidade->umidade = $result['umidade'];
            $umidade->setTime($result['data']);
            return $umidade;
        }

        $this->conClose();
    }

    public function getGraph($planta){
        $this->conOpen();
        $query = "SELECT * FROM Umidade WHERE planta = \"$planta\" ORDER BY id DESC LIMIT 10;";
        if($result = mysqli_query($this->con, $query)){
            $results = array();
            while ($row = $result->fetch_assoc()) {
                $g = new Graph;
                $g->umidade = $row['umidade'];
                $data = strtotime($row["data"]);
                $g->data = date("H/i/s", $data);
                $results[] = $g;
            }
            return json_encode($results);
        }

        $this->conClose();
    }

    public function insert(Umidade $u){
        $this->conOpen();
        $id = null;
        $query = "INSERT INTO Umidade (planta,umidade,data) VALUES (\"$u->planta\", $u->umidade, \"$u->data\");";
        if(($this->con)->query($query) === TRUE){
            $u->id = ($this->con)->insert_id;
            $this->conClose();
            return $u;
        }else{
            return $query;
        }
    }

    public function conOpen(){
        $this->con = new mysqli("localhost", "root", "pedro123", "irrigacao");
    }

    public function conClose(){
        mysqli_close($this->con);
    }
}

 ?>
